package mx.tecnm.misantla.listviewapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var frutas: ArrayList<String> = ArrayList()
        frutas.add("Limon")
        frutas.add("Manzana")
        frutas.add("Naranja")
        frutas.add("Uvas")
        frutas.add("kiwis")

        frutas.add("Banana")
        frutas.add("Mango")
        frutas.add("Frambueza")
        frutas.add("Sandia")

        val lista = findViewById<ListView>(R.id.lista)

        val adaptador = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, frutas)

        lista.adapter = adaptador

        lista.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            Toast.makeText(this, frutas.get(i), Toast.LENGTH_LONG).show()
            if (i == 0) {
                val intent = Intent(this, LemonActivity::class.java)
                startActivity(intent)
            } else if (i == 1) {
                val intent = Intent(this, AppleActivity::class.java)
                startActivity(intent)
            } else if (i == 2) {
                val intent = Intent(this, OrangeActivity::class.java)
                startActivity(intent)
            } else if (i == 3) {
                val intent = Intent(this, GrapeActivity::class.java)
                startActivity(intent)
            } else if (i == 4) {
                val intent = Intent(this, KiwiActivity::class.java)
                startActivity(intent)
            } else if (i == 5) {
                val intent = Intent(this, BananaActivity::class.java)
                startActivity(intent)
            } else if (i == 6) {
                val intent = Intent(this, MangoActivity::class.java)
                startActivity(intent)
            } else if (i == 7) {
                val intent = Intent(this, FrambuezaActivity::class.java)
                startActivity(intent)
            } else {
                val intent = Intent(this, SandiaActivity::class.java)
                startActivity(intent)
            }
        }


    }
}